import axios from 'axios';

// call get api with url 
export const callgetAPI = (url) => {
    return axios.get(url)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error;
        });
}

// call post api with url and data
export const callpostAPI = (url, data) => {
    return axios.post(url, data)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error;
        });
}