// function to set key/value inside the local storage
export const setLocalStorage = (name, value) => {
    localStorage.setItem(name, value);
}

// function to clear local storage data 
export const clearLocalStorage = () => {
    localStorage.clear();
}

// function to check if any key present in localstorage or not 
export const checkLocalStorage = () => {
    if (localStorage && Object.keys(localStorage).length > 0) {
        return true;
    } else {
        return false;
    }
}