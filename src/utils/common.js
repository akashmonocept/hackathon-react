import * as moment from 'moment';

// function to filter array based on field name and value
export const filter = (array, field, value) => {
    let filteredArray = [];
    if (array && array.length > 0) {
        filteredArray = array.filter(item => {
            if(field in item && item[field]){
                if(item[field].toLowerCase().includes(value.toLowerCase())){
                    return true;
                }
            }
            return false; 
        });
    }
    return filteredArray;
}

// function to filter array based on field name, start date and end date
export const filterByDate = (array, field, start, end) => {
    let filteredArray = [];
    if (array && array.length > 0) {
        filteredArray = array.filter(item => {
            if(field in item && item[field]){
                if (start <= moment(item[field]).valueOf() &&  moment(item[field]).valueOf() <= end) {
                    return true;
                }
            }
            return false;
        });
    }
    return filteredArray;
}