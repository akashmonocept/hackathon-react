import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Login from './components/login/login';
import Header from './components/header/header';
import Dashboard from './components/dashboard/dashboard';
import Activity from './components/activity/activity';
import { checkLocalStorage } from './utils/localStorage';

// function to check if username and password present inside the localstorage or not
function isLoggedIn() {
  if (checkLocalStorage()) {
    return true;
  } else {
    return false;
  }
}

function App() {
  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/dashboard" render={() => {
          if (isLoggedIn()) {
            return (<Dashboard />)
          } else {
            return (<Redirect to="login" />)
          }
        }}>
        </Route>
        <Route exact path="/activity/:id" render={() => {
          if (isLoggedIn()) {
            return (<Activity />)
          } else {
            return (<Redirect to="/login" />)
          }
        }}>
        </Route>
        {
          isLoggedIn() ? (<Redirect from="/" to="dashboard" />):(<Redirect to="/login" />)
        }
      </Switch>
    </Router>
  );
}

export default App;
