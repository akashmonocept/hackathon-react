import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class Activity extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activity: {}
        }
    }

    // function to redirect to dashboard
    redirectToDashboard = () => {
        this.props.history.push('/dashboard');
    }

    componentWillMount() {
        if (this.props.location.state) {
            this.setState({
                activity: this.props.location.state
            })

        }
    }

    render() {
        return (
            // get activity details
            <div className="container containerHght">
                 <button className="btn btn-success buttnWidth" type="button" onClick={this.redirectToDashboard}> <i className="fa fa-arrow-left mr-2" aria-hidden="true"></i>Back</button>
                <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-12">
                        <ul className="list-group mt-5 card cardBackground">
                            <h5 className="card-title header"><i className="fa fa-money mr-1" aria-hidden="true"></i>Payment Details</h5>

                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Account Number
                    <span className="badge badge-pill taskColor">
                   {this.state.activity.paymentDetails.accountNumber}
                   </span>
                      </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                IFSC Code
                 <span className="badge badge-pill taskColor">
                                    {this.state.activity.paymentDetails.ifscCode}
                                </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Bank Name
                <span className="badge badge-pill taskColor">
                    {this.state.activity.paymentDetails.bankName}
                    </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Branch Name
                <span className="badge badge-pill">
                    {this.state.activity.paymentDetails.branchName}
                    </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Account Holder Name
                         <span className="badge badge-pill">
                        {this.state.activity.paymentDetails.accountHolderName}
                      </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Status
                    <span className="badge badge-pill">
                    {this.state.activity.paymentDetails.status}
                    </span>
                            </li>
                        </ul>
                    </div>
                    {/* User Details */}

                    <div className="col-lg-6 col-md-6 col-sm-12">
                        <ul className="list-group mt-5 card cardBackground">
                            <h5 className="card-title header"><i className="fa fa-info-circle mr-1" aria-hidden="true"></i>User Details</h5>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Name
                           <span className="badge badge-pill">
                            {this.state.activity.userDetails.name}
                            </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Contact
                        <span className="badge badge-pill">
                            {this.state.activity.userDetails.contact}
                            </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Address
                          <span className="badge badge-pill">
                            {this.state.activity.userDetails.address}
                            </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                City
                       <span className="badge badge-pill">
                           {this.state.activity.userDetails.city}
                           </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                District
                     <span className="badge badge-pill">
                        {this.state.activity.userDetails.district}
                        </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                State
                        <span className="badge badge-pill">
                            {this.state.activity.userDetails.state}
                            </span>
                            </li>
                        </ul>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 cardPadding">
                        <ul className="list-group mt-5 card cardBackground">
                            <h5 className="card-title header"><i className="fa fa-tasks mr-1" aria-hidden="true"></i>Task Details</h5>

                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Task Name
                         <span className="badge badge-pill taskColor">
                             {this.state.activity.taskDetails.name}
                             </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Priority
                            <span className="badge  badge-pill taskColor">
                                {this.state.activity.taskDetails.priority}
                                </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Status
                    <span className="badge badge-pill">
                        {this.state.activity.taskDetails.status}
                        </span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center listBorder">
                                Task ID
                          <span className="badge badge-pill">
                              {this.state.activity.taskDetails.taskId}
                              </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Activity);
