import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {clearLocalStorage, checkLocalStorage} from '../../utils/localStorage';

class Header extends Component {

    // function to redirect to dashboard
    redirectToDashboard = () => {
        this.props.history.push('/login');
    }

    // function to be called on click of logout button
    logout = (event) => {
        event.preventDefault();
        // function to clear local storage 
        clearLocalStorage();
        this.redirectToDashboard();
    }
    // redirect to dashboard
    gotTodashboard = () => {
        this.props.history.push('/dashboard');
    }
    render() {
        return (
            <div>
                {/* Header starts here */}
                <nav className="navbar navbar-dark bg-dark">
                    <img src="/logo.png" width="90" height="60" alt="hackthex"/>
                    {
                        checkLocalStorage() ? (
                            <button className="btn btn-success" type="button" onClick={this.logout}>Logout</button>
                        ) : null
                    }
                    
                </nav>
                {/* Header ends here */}
            </div>
        );
    }
}

export default withRouter(Header);
