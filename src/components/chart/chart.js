import React  from 'react';
import { PieChart } from 'react-chartkick'
import 'chart.js'

// function to render pie chart on UI
export const Piechart = ({ label }) => {
    return (
        <PieChart data={label} />
    );
};
