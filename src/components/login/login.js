import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {setLocalStorage} from '../../utils/localStorage';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginError: false
        }
    }

    // function to be called on login form submit
    onLoginFormSubmit = (event) => {
        event.preventDefault();
        // using default username as admin and password as admin
        if (event.target.username.value === 'admin' && event.target.password.value === 'admin') {
            setLocalStorage('username',event.target.username.value);
            setLocalStorage('password',event.target.password.value);
            this.props.history.push('/dashboard');
        } else {
            this.setState({
                loginError: true
            });
        }
    }

    // function to show error if username or password is incorrect
    loginError = () => {
        return (
            <div className="alert alert-danger text-center" role="alert">
                Please check your username and password !
            </div>
        )
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-3"></div>
                    <div className="card mt-20 col-sm-6">
                        <div className="card-body">
                            <form onSubmit={this.onLoginFormSubmit}>
                                <div className="form-group">
                                    <i class="fa fa-user fa-lg" aria-hidden="true"></i>
                                    <label className="ml-1"><b>Username</b></label>
                                    <input className="form-control" type="text" required name="username" />
                                </div>
                                <div className="form-group">
                                    <i class="fa fa-unlock-alt fa-lg" aria-hidden="true"></i>
                                    <label className="ml-1"><b>Password</b></label>
                                    <input className="form-control" type="password" required name="password" />
                                </div>
                                {this.state.loginError && this.loginError()}
                                <button type="submit" className="btn btn-success">Login</button>
                            </form>
                        </div>
                    </div>
                    <div className="col-sm-3"></div>
                </div>
            </div>
        );
    }
}

export default withRouter(Login);
