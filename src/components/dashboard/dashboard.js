import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import { callgetAPI, callpostAPI } from '../../utils/request';
import { filter, filterByDate } from '../../utils/common';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import * as moment from 'moment';
import {TASKS_COUNT_URL, TASK_DETAILS_URL} from '../../env.config';
import { Piechart } from '../chart/chart';
const LOADER_ICON = "https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif";

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            allActivities: [],
            selectedFilter: 'finalStatus',
            activities: [],
            startDate: new Date(),
            endDate: new Date(),
            dateFilterError: false,
            loader: true,
            allActivitiesCount: [],
            selectedTaskName: '',
            pageNo: 0,
            total: 10,
            lastPage: false,
            pieChartData: []
        };
    }

    // get task by name by calling API
    getTasksByName = (name) => {
        let getTaskByName = {
            "request": {
                "masterDetails": {
                    "taskDetails":{
                        "name":""
                    }
                },
                "additionalInfo": {
                    "pageNo": "0",
                    "totalData": "10"

                }
            }
        }
        getTaskByName["request"]["masterDetails"]["taskDetails"]["name"] = name;
        getTaskByName["request"]["additionalInfo"]["pageNo"] = this.state.pageNo;
        getTaskByName["request"]["additionalInfo"]["totalData"] = this.state.total;
        callpostAPI(TASK_DETAILS_URL, getTaskByName).then(response => {
            if (response && 'data' in response && response.data && 'response' in response.data && response.data.response && 'masterDetailsList' in response.data.response && response.data.response.masterDetailsList && 'content' in response.data.response.masterDetailsList && response.data.response.masterDetailsList.content && response.data.response.masterDetailsList.content.length > 0 ) {
                this.setState({
                    allActivities: response.data.response.masterDetailsList.content,
                    activities: response.data.response.masterDetailsList.content,
                    selectedTaskName: name,
                    lastPage: response.data.response.masterDetailsList.last
                });
            }
        });
    }

    // function to create pie chart data
    createPieChartData = (tasks) => {
        let pieChartData = [];
        for(let task of tasks){
            console.log(task);
            if(('name' in task && task['name']) && ('total' in task && task['total'])){
                pieChartData.push([task.name, task.total]);
            }
        }
        if(pieChartData && pieChartData.length > 0){
            this.setState({
                pieChartData: pieChartData
            });
        }
    }
    

    componentWillMount() {
        callgetAPI(TASKS_COUNT_URL).then(response => {
            console.log("Response", response);
            if (response && 'data' in response && response.data && 'response' in response.data && response.data.response && 'dashboardResult' in response.data.response && response.data.response.dashboardResult.length > 0) {
                this.createPieChartData(response.data.response.dashboardResult);
                this.getTasksByName(response.data.response.dashboardResult[0].name);
                this.setState({
                    allActivitiesCount: response.data.response.dashboardResult,
                    loader: false
                });
            } else {
                this.setState({
                    loader: false
                });
            }
        });
    }

    prevPage = (event) => {
        event.preventDefault();
        if(this.state.pageNo > 0){
            this.setState({
                pageNo: this.state.pageNo - 1
            }, ()=>{
                this.getTasksByName(this.state.selectedTaskName);
            });
        }
    }

    nextPage = (event) => {
        event.preventDefault();
        this.setState({
            pageNo: this.state.pageNo + 1
        }, ()=>{
            this.getTasksByName(this.state.selectedTaskName);
        });
    }

    selectPageTotal = (event) => {
        event.preventDefault();
        this.setState({
            pageNo: 0, 
            total: event.target.value
        }, ()=>{
            this.getTasksByName(this.state.selectedTaskName);
        });
    }

    // function to handle filter radio button selection on dashboard screen
    handleOptionChange = (event) => {
        this.setState({
            selectedFilter: event.target.value
        });
    }

    // function to be called on select of an activity from table
    selectActivity = (event, activity) => {
        event.preventDefault();
        if ('id' in activity && activity.id) {
            this.props.history.push({
                pathname: `/activity/${activity.id}`,
                state: activity
            });
        }
    }

    // function to be called on value changes inside the filter value input field
    handleFilterSubmit = (event) => {
        event.preventDefault();
        let filteredData = []
        if (event.target.value === '') {
            filteredData = this.state.activities;
        } else {
            filteredData = filter(this.state.allActivities, this.state.selectedFilter, event.target.value);
        }
        this.setState({
            allActivities: filteredData
        });
    }

    // function to handle start or from date from date picker inside the filter section 
    handleStartDateChange = (date) =>{
        this.setState({
            startDate: date,
            allActivities: this.state.activities
        });
    }

    // function to handle end or to date from date picker inside the filter section 
    handleEndDateChange = (date) =>{
        this.setState({
            endDate: date,
            allActivities: this.state.activities
        });
    }

    // function to be called on submit of start/from and end/to date filters
    handleFilterByDate = (event) =>{
        event.preventDefault();
        // start date in milliseconds
        let startDateInMillis = moment(this.state.startDate).startOf('day').valueOf();
        // end date in milliseconds
        let endDateInMillis = moment(this.state.endDate).endOf('day').valueOf();
        // checking if end date is smaller than start date 
        if (endDateInMillis < startDateInMillis) {
            this.setState({
                dateFilterError: true
            });
        } else {
            let filteredData = filterByDate(this.state.allActivities, 'startTime', startDateInMillis, endDateInMillis);
            this.setState({
                allActivities: filteredData,
                dateFilterError: false
            });
        }
    }

    // function to show error if end date is smaller than start date
    showDateFilterError = () => {
        return (
            <div className="alert alert-danger text-center" role="alert">
                End date cannot be smaller than start date !
            </div>
        )
    }

    // function to select task name to fetch all task by name
    selectedTaskName = (event, activity) => {
        event.preventDefault();
        if('name' in activity && activity.name){
            this.getTasksByName(activity.name);
        }
    }

    render() {
        return (
            <div className="container">
                <div className="mt-3">
                    {/* Filter section starts here */}
                        <div className="accordion">
                            <div className="border">
                                <div className="alert alert-secondary mb-0" id="headingOne">
                                    <h5 className="mb-0">
                                        <i className="fa fa-filter" aria-hidden="true"></i> FILTER
                                </h5>
                                </div>
                                <div className="collapse show">
                                    <div className="card-body">
                                        <form>
                                            <div className="d-inline-flex">
                                                <div className="radio ml-2">
                                                    <label>
                                                        <input type="radio" value="finalStatus" checked={this.state.selectedFilter === 'finalStatus'} onChange={this.handleOptionChange} />
                                                        <span className="badge badge-danger ml-1">STATUS</span>
                                                    </label>
                                                </div>
                                                <div className="radio ml-2">
                                                    <label>
                                                        <input type="radio" value="date" checked={this.state.selectedFilter === 'date'} onChange={this.handleOptionChange} />
                                                        <span className="badge badge-info ml-1">DATE</span>
                                                    </label>
                                                </div>
                                            </div>

                                            {
                                                (this.state.selectedFilter !== 'date') && (
                                                    <div className="form-group">
                                                        <input className="form-control mt-2" type="text" required name="filter" placeholder="Please enter the keyword to filter the records" onChange={this.handleFilterSubmit} />
                                                    </div>
                                                )
                                            }

                                            {
                                                (this.state.selectedFilter === 'date') && (
                                                    <div>
                                                        <div className="row mt-4">
                                                            <div className="form-group col-md-5">
                                                                <i className="fa fa-calendar fa-lg mr-2" aria-hidden="true"></i>
                                                                <span className="badge badge-success mr-1">FROM</span>
                                                                <DatePicker
                                                                    selected={this.state.startDate}
                                                                    onChange={this.handleStartDateChange}
                                                                    maxDate={new Date()}
                                                                />
                                                            </div>

                                                            <div className="form-group col-md-5">
                                                                <i className="fa fa-calendar fa-lg mr-2" aria-hidden="true"></i>
                                                                <span className="badge badge-success mr-1">TO</span>
                                                                <DatePicker
                                                                    selected={this.state.endDate}
                                                                    onChange={this.handleEndDateChange}
                                                                    maxDate={new Date()}
                                                                />
                                                            </div>
                                                        </div>
                                                        {
                                                            this.state.dateFilterError && this.showDateFilterError()
                                                        }
                                                        <button onClick={this.handleFilterByDate} className="btn btn-dark mt-2"> <i className="fa fa-search" aria-hidden="true"></i> SEARCH</button>
                                                    </div>
                                                )
                                            }


                                        </form>
                                    </div>
                                </div>
                            </div>
                    </div>
                    {/* Filter section ends here */}

                    {/* Task name section starts here */}
                    {
                        <div className="mt-3">
                            <h6 className="text-danger">* PLEASE SELECT ANY TASK FROM BELOW : </h6>
                            {
                                this.state.allActivitiesCount && this.state.allActivitiesCount.length > 0 ? (
                                    this.state.allActivitiesCount.map((activity, index) => {
                                        return (
                                            (activity.name === this.state.selectedTaskName) ? (<button key={index} type="button" className="btn btn-success ml-2 mt-2" onClick={(e) => this.selectedTaskName(e, activity)}>
                                                {activity.name} <span className="badge badge-light ml-1">{activity.total}</span>
                                            </button>):(
                                                <button key={index} type="button" className="btn btn-secondary ml-2 mt-2" onClick={(e) => this.selectedTaskName(e, activity)}>
                                                    {activity.name} <span className="badge badge-light ml-1">{activity.total}</span>
                                                </button>
                                            )
                                        )})
                                ) : null 
                            }                           
                        </div>
                    }

                    {/* Task name sections end here */}

                    <div className="mt-3">
                        <div className="alert alert-success" role="alert">
                            You have selected this task : <b>{this.state.selectedTaskName}</b>
                        </div>
                    </div>

                    {/* Items per page starts here */}
                    <b>Items per page :</b>
                    <select className="ml-1" value={this.state.total} onChange={(e) => this.selectPageTotal(e)}>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>

                    {/* Items per page ends here */}

                    <nav aria-label="Page navigation example">
                        <ul className="pagination justify-content-end m-30">
                            <button className="btn btn-success" disabled={(this.state.pageNo === 0)} onClick={this.prevPage}>
                                <i className="fa fa-arrow-left" aria-hidden="true"></i> Previous
                            </button>

                            <button className="btn btn-success ml-2" disabled={this.state.lastPage} onClick={this.nextPage}>
                                Next <i className="fa fa-arrow-right" aria-hidden="true"></i>
                            </button>
                        </ul>
                    </nav>

                    {/* List of activities/tasks starts here */}
                    {this.state.allActivities && this.state.allActivities.length > 0 ?
                        (
                            <div style={{ 'overflowX': 'auto' }}>
                                <table className="table table-bordered table-hover mt-3">
                                    <thead className='thead-dark'>
                                        <tr>
                                            <th scope="col">S.No.</th>
                                            <th scope="col">ID</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Start Date</th>
                                            <th scope="col">Status</th>
                                        </tr>
                                    </thead>
                                    {
                                        this.state.allActivities.map((activity, index) => {
                                            return (
                                                <tbody key={index}>
                                                    <tr style={{ 'cursor': 'pointer' }} onClick={(e) => this.selectActivity(e, activity)}>
                                                        <td>{index + 1}</td>
                                                        <td>{activity.id}</td>
                                                        <td>{activity.taskDetails.name}</td>
                                                        <td>{moment(activity.startTime).format('MMMM Do YYYY, h:mm:ss a')}</td>
                                                        {
                                                            activity.finalStatus === 'In-Progress' && (<td className="text-warning">
                                                                {activity.finalStatus}
                                                            </td>)
                                                        }
                                                        {
                                                            activity.finalStatus === 'Pass' && (<td className="text-success">
                                                                {activity.finalStatus}
                                                            </td>)
                                                        }
                                                        {
                                                            (activity.finalStatus === 'Fail' || activity.finalStatus == null) && (<td className="text-danger">
                                                                Fail
                                                </td>)
                                                        }
                                                    </tr>
                                                </tbody>
                                            )
                                        })
                                    }
                                </table>
                            </div>) : (
                            !this.state.loader && <div className='text-center mt-5 text-danger'>
                                <h3>  No Record Found </h3>
                            </div>
                        )
                    }
                    { 
                        this.state.loader && 
                        (
                            <div className="container text-center">
                                <img src={LOADER_ICON} alt="loading..." />
                            </div>
                        )
                    }
                    {/* List of activities/tasks ends here*/}
                    
                    {/* Tasks visual representation starts here */}

                    {
                        this.state.pieChartData.length > 0 && <div className="container mt-4 mb-3 border border-secondary p-3">
                            <h5> <i class="fa fa-pie-chart" aria-hidden="true"></i> TASKS VISUAL REPRESENTATION</h5>
                            <br/>
                            <Piechart label={this.state.pieChartData} />
                        </div>
                    }

                    {/* Tasks visual representation ends here */}
                </div >
            </div >
        );
    }
}

export default withRouter(Dashboard);
